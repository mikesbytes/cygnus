## Building cygnus (and cygnus-tui) from source

The official builds are created on an Ubuntu 18.04 system using GCC9. It has succesfully compiled under other systems
and compilers but remains largely untested.

System Dependency Requirements:
- CMake >= 3.1
- conan
- sqlite3

cygnus and cygnus-tui require packages not available in the default conan repos. If you are building a stable version,
you can use the conan repo [here](https://bintray.com/maporter/mikesbytes).

If you are building a development version or wish to build all dependencies from the source, you will need:

- [libcygnus](https://gitlab.com/mikesbytes/libcygnus)
- [taglib](https://gitlab.com/jonesconrad1/conan-taglib)
- [concurrentqueue](https://gitlab.com/mikesbytes/conan-concurrentqueue)
- [cpptui](https://gitlab.com/mikesbytes/cpptui) (only required for cygnus-tui)

The packages can be built by cloning their respective repos and running `conan create .` within them

To clone and build either cygnus or cygnus-tui, follow standard CMake procedure

**cygnus:**
```
git clone --recursive https://gitlab.com/mikesbytes/cygnus.git
cd cygnus
mkdir build && cd build
cmake ..
make
```

**cygnus-tui:**
```
git clone https://gitlab.com/mikesbytes/cygnus-tui.git
cd cygnus-tui
mkdir build && cd build
cmake ..
make
```

Currently there are no install directives and the executables are just dumped into a `bin` folder inside your build
folder