//
// Created by user on 6/9/19.
//

#include <iostream>
#include "threadpool.hpp"
#include "connectionmanager.hpp"

#include <boost/program_options.hpp>

namespace po = boost::program_options;

int main(int argc, char* argv[]) {
    // settings
    std::string host = "127.0.0.1";
    int port = 6321;
    std::vector<std::string> commands;
    bool keepalive = false;
    bool norespond = false;
    int timeout = 500;

    // handle program args
    po::options_description desc("Options");
    desc.add_options()
            ("host,h", po::value<std::string>(), "host to connect to")
            ("port,p", po::value<int>(), "port to connect to")
            ("help", "produce help message")
            ("keepalive,l", "stay open for live responses (overrides norespond)")
            ("norespond,n", "don't wait for a response")
            ("json,j",po::value<std::vector<std::string>>(), "json commands to send")
            ;
    po::variables_map vm;
    try {
        po::store(po::parse_command_line(argc,argv,desc), vm);

        if (vm.count("help")) {
            std::cout << desc << std::endl;
        }

        if (vm.count("host")) {
            host = vm["host"].as<std::string>();
        }

        if (vm.count("port")) {
            port = vm["port"].as<int>();
        }

        if (vm.count("keepalive")) {
            keepalive = true;
        }

        if (vm.count("norespond")) {
            norespond = true;
        }

        if (vm.count("json")) {
            auto cmds = vm["json"].as<std::vector<std::string>>();
            for (auto &i : cmds) {
                commands.push_back(i);
            }
        }
    } catch (std::exception &e) {
        std::cout << e.what() << std::endl;
    }

    for (auto &i : commands) {
        std::cout << i << std::endl;
    }

    /*
    ThreadPool tp;
    tp.spawnWorkers(2);
     */

    ConnectionManager cm;

    // request processing fn
    auto process_msg = [](ClientCtxPtr, uint8_t tag, DataWrapper data) {
        std::cout << data.getString() << std::endl;
    };
    cm.processMessageSignal.connect(process_msg);


    auto ctx = cm.connectTo(host, port);

    for (int i = 0; i < commands.size(); ++i)
        ctx->send(commands[i]);

    if (!norespond) cm.process(timeout);

    while(keepalive)
        cm.process(timeout);
}