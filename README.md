## Cygnus Music Server

*Note: This project is in heavy development and is not yet intended for end users*

### What is it?

Cygnus could most simply be described as a music player.

Unlike a regular music player however, Cygnus operates as a server/client system. You run the server to handle your
music library and playback, and you use clients to control it. Clients work both locally and across the net.

The project includes the server and a CLI based client (cygnusc). Check out cygnus-tui for a well-featured
TUI based client. GUI and mobile clients will be coming sometime down the line.

### Build instructions

Cygnus uses the Conan dependency manager, you will need this. If you are building the stable branch, all the
dependencies are available in the default conan repositories and my custom repository located [here](https://bintray.com/maporter/mikesbytes).

If you are building the development branch, you will likely need to build the development versions of 
[libcygnus](https://gitlab.com/mikesbytes/libcygnus) and [cpptui](https://gitlab.com/mikesbytes/cpptui).
They include conan packaging scripts as well and can be installed by cloning and running `conan create` in the cloned dir.

After ensuring the dependencies are installed, you can build the cygnus server.

```
git clone https://gitlab.com/mikesbytes/cygnus.git

cd cygnus

mkdir build && cd build

conan install ..

cmake .. && make
```