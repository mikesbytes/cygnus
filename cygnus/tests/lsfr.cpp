//
// Created by user on 2/8/20.
//

#include <catch.hpp>

#include "../lsfr.hpp"

TEST_CASE( "LSFR based PRNG", "[LSFR]") {
    LSFR rand;
    LSFR next = rand.getNext();

    REQUIRE(rand.getValue() != next.getValue());
    REQUIRE(rand.getNext().getValue() == next.getValue());
}
