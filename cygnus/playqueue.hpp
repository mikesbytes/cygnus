//
// Created by user on 6/18/19.
//

#ifndef CYGNUS_PLAYQUEUE_HPP
#define CYGNUS_PLAYQUEUE_HPP

#include <vector>
#include <string>
#include "clientcontext.hpp"
#include "library.hpp"
#include "lsfr.hpp"

#include <boost/signals2/signal.hpp>
#include <spdlog/spdlog.h>
#include <set>

class SubscriptionManager;

class PlayQueue {
public:
    PlayQueue(SubscriptionManager &subManager, Library &mLibrary);

    void addTrack(const std::string& trackName);
    bool hasNext();
    /// get the index of the next track
    // note, does NOT obey consume
    int nextIndex();
    std::optional<std::string> nextTrack();

    int skipTo(int index);

    int skipRelative(int rel_index, bool no_shuffle = false);
    bool skipToNext();
    bool empty();

    std::string currentTrack();
    int currentIndex() const;

    bool removeTrack(int index);

    int clear();

    json getListing();

    json currentTrackInfo();
    json trackInfo(int index);
    void updateSubscriptions();

    int queueTracks(const std::string &source, const std::string &path);
    json enqueue(const json &args);

    std::optional<std::string> getTrackAt(int index);

    bool getShuffle() const;

    bool setShuffle(bool shuffle);
    void resetUnplayedTracks();

    bool setPlaybackFlags(const json &j);
    json getPlaybackFlags();

    // signals
    boost::signals2::signal<void()> stopSignal;
    boost::signals2::signal<void(const std::string &uri)> trackChangedSignal;

protected:
    void mTrackChangedSubscription();

    std::vector<std::string> mTracks;
    std::set<int> unplayed_tracks_;
    int mIndex;
    SubscriptionManager &mSubManager;
    Library &mLibrary;
    std::shared_ptr<spdlog::logger> logger_;

    LSFR shuffle_rand_;
    bool shuffle_;
    bool shuffle_repeat_;
    bool consume_;
    bool single_;
    bool loop_;
};


#endif //CYGNUS_PLAYQUEUE_HPP
