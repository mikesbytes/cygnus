//
// Created by user on 6/19/19.
//

#ifndef CYGNUS_SUBSCRIPTIONMANAGER_HPP
#define CYGNUS_SUBSCRIPTIONMANAGER_HPP

#include <map>
#include <vector>

#include "clientcontext.hpp"

class Socket;

class SubscriptionManager {
public:
    SubscriptionManager();

    void sendUpdate(const std::string& tag, json& doc);

    void subscribeAction(std::shared_ptr<ClientContext> ctx, uint8_t tag, const json &doc);

protected:
    std::map<std::string, std::vector<ClientCtxPtr>> mSubscribers;
};


#endif //CYGNUS_SUBSCRIPTIONMANAGER_HPP
