//
// Created by user on 12/19/19.
//

#ifndef CYGNUS_APPCONTEXT_HPP
#define CYGNUS_APPCONTEXT_HPP


#include <toml11/toml/value.hpp>
#include "threadpool.hpp"
#include "library.hpp"
#include "subscriptionmanager.hpp"
#include "playqueue.hpp"
#include "audioprovider.hpp"
#include "output.hpp"

class AppContext {
public:
    AppContext(const toml::value &conf);

    ThreadPool &getThreadPool();
    Library &getLibrary();
    SubscriptionManager &getSubscriptionManager();
    PlayQueue &getPlayQueue();
    AudioProvider &getAudioProvider();
    Output &getOutput();
private:
    ThreadPool &mThreadPool;
    Library mLibrary;
    SubscriptionManager mSubscriptionManager;
    PlayQueue mPlayQueue;
    AudioProvider mAudioProvider;
    Output mOutput;
    toml::value dummy_conf_;
};


#endif //CYGNUS_APPCONTEXT_HPP
