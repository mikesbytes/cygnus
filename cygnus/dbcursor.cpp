//
// Created by user on 12/21/19.
//

#include <stdexcept>
#include <string>

#include "dbcursor.hpp"

DBCursor::DBCursor(sqlite3_stmt *stmt) :
        mSTMT(stmt),
        mFinalized(false)
{
    step();
}

void DBCursor::step() {
    mRC = sqlite3_step(mSTMT);
    if (mRC == SQLITE_BUSY) {
        throw std::runtime_error("SQLite DB is busy");
    } else if (mRC == SQLITE_ERROR) {
        throw std::runtime_error("SQLite error during step");
    } else if (mRC == SQLITE_MISUSE) {
        throw std::runtime_error("SQLite misuse during step");
    }
}

bool DBCursor::isRow() {
    return (mRC == SQLITE_ROW);
}

bool DBCursor::isDone() {
    return (mRC == SQLITE_DONE);
}

int DBCursor::columnCount() {
    return sqlite3_column_count(mSTMT);
}

const char * DBCursor::columnName(int index) {
    return sqlite3_column_name(mSTMT, index);
}

DBCursor::~DBCursor() {
    if (!mFinalized)
        sqlite3_finalize(mSTMT);
}

int DBCursor::getInt(int col) {
    if (!isRow()) throw std::runtime_error("SQL failure, not a row");
    return sqlite3_column_int(mSTMT, col);
}

const unsigned char *DBCursor::getText(int col) {
    return sqlite3_column_text(mSTMT, col);
}

std::string DBCursor::getString(int col) {
    return std::string(reinterpret_cast<const char*>(getText(col)));
}

DBCursor::DBCursor(DBCursor &&other) :
    mSTMT(other.mSTMT),
    mRC(other.mRC),
    mFinalized(other.mFinalized)
{
    other.mSTMT = nullptr;
    other.mFinalized = true;
    other.mRC = 0;
}

