//
// Created by user on 6/18/19.
//

#include "playqueue.hpp"
#include "subscriptionmanager.hpp"
#include "metadatareader.hpp"
#include "doctools.hpp"
#include "metadatatools.hpp"

PlayQueue::PlayQueue(SubscriptionManager &subManager, Library &mLibrary) :
        mIndex(-1),
        mSubManager(subManager),
        mLibrary(mLibrary),
        shuffle_(false),
        shuffle_repeat_(false),
        consume_(false),
        single_(false),
        loop_(false)
        {

    trackChangedSignal.connect(boost::bind(&PlayQueue::mTrackChangedSubscription, this));
    logger_ = spdlog::get("cygnus");

}
void PlayQueue::addTrack(const std::string &trackName) { mTracks.push_back(trackName);
    unplayed_tracks_.insert(mTracks.size() - 1);
    logger_->info("queueing track: {}", trackName);
}


int PlayQueue::skipTo(int index) {
    int new_index;
    int old_index = mIndex;


    // stop if the new index is out of bounds
    if (index < 0 || index >= mTracks.size()) {
        new_index = -1;
        resetUnplayedTracks();
        stopSignal();
    } else {
        new_index = index;
    }

    mIndex = new_index;
    unplayed_tracks_.erase(mIndex);

    if (consume_) {
        removeTrack(old_index);
    }

    trackChangedSignal(currentTrack());

    logger_->info("skipping to track: {}", index);
    return mIndex;
}

int PlayQueue::skipRelative(int rel_index, bool no_shuffle) {
    int next = mIndex;
    if (shuffle_ && !no_shuffle) {
        if (rel_index < 0) {
            return -1;
        }

        for (int i = 0; i < rel_index; ++i){
            next = nextIndex();
            shuffle_rand_ = shuffle_rand_.getNext();
        }
    } else {
        next += rel_index;
    }

    return skipTo(next);
}

bool PlayQueue::skipToNext() {
    int next_index = nextIndex();
    skipTo(next_index);
    if (single_ && !loop_) stopSignal();
    return true;
}

bool PlayQueue::empty() {
    return mTracks.empty();
}

bool PlayQueue::hasNext() {
    return nextIndex() != -1;
}

std::string PlayQueue::currentTrack() {
    if (mIndex < 0 || mIndex >= mTracks.size()) {
        return "";
    }
    return mTracks[mIndex];
}

void PlayQueue::updateSubscriptions() {
}

json PlayQueue::getListing() {
    json j = {{"index", currentIndex()},
              {"entries", {}}};

    for (auto &i : mTracks) {
        j["entries"].push_back(mLibrary.getTrackMetadata(i).value());
    }
    return j;
}


bool PlayQueue::removeTrack(int index) {
    if (index < 0 || index >= mTracks.size()) {
        return false;
    }

    if (index == mIndex) {
        stopSignal();
        mIndex = -1;
    }

    if (index < mIndex) {
        --mIndex;
    }

    mTracks.erase(mTracks.begin() + index);
    unplayed_tracks_.erase(index);

    // TODO: see if this code can be made more performant

    // make sure unplayed tracks stays consistent
    std::set<int> affected_unplayed_tracks;
    for (auto it = unplayed_tracks_.begin(); it != unplayed_tracks_.end(); ++it) {
        if (*it > index) {
            affected_unplayed_tracks.insert(*it);
        }
    }
    for (auto it = affected_unplayed_tracks.begin(); it != affected_unplayed_tracks.end(); ++it) {
        unplayed_tracks_.erase(*it);
    }
    for (auto it = affected_unplayed_tracks.begin(); it != affected_unplayed_tracks.end(); ++it) {
        unplayed_tracks_.insert(*it - 1);
    }

    json j = {{"message-type","update"},
              {"update-type","queue"},
              {"data",{
                                      {"mode","remove"},
                                      {"index", index},
                                      {"count",1}
                              }}};

    mSubManager.sendUpdate("queue",j);

    logger_->info("removing track: {}", index);
    return true;
}

int PlayQueue::clear() {
    if (mIndex >= 0) {
        skipTo(-1);
    }

    int ret = mTracks.size();
    mTracks.clear();
    unplayed_tracks_.clear();

    json j = {{"message-type","update"},
              {"update-type","queue"},
              {"data",{
                                      {"mode","clear"}
                              }}};

    mSubManager.sendUpdate("queue",j);
    logger_->info("clearing tracks");
    return ret;
}

int PlayQueue::currentIndex() const {
    return mIndex;
}

void PlayQueue::mTrackChangedSubscription() {
    json j = {
            {"message-type", "update"},
            {"update-type", "track-change"},
            {"data",getFileMetadata(mLibrary.getTrackPath(currentTrack())).value()}
    };
    j["data"]["index"] = currentIndex();

    mSubManager.sendUpdate("track-change", j);
}

int PlayQueue::queueTracks(const std::string &source, const std::string &path) {
    auto tracks = mLibrary.getTracksInPath(source, path);
    json j = {{"message-type","update"},
              {"update-type","queue"},
              {"data",{
                                      {"mode","append"},
                                      {"entries",{}}
                              }}};
    for (auto &i : tracks) {
        addTrack(i);
        j["data"]["entries"].push_back(mLibrary.getTrackMetadata(i).value());
        logger_->info("queueing track: {}", i);
    }
    mSubManager.sendUpdate("queue", j);
    updateSubscriptions();
    return tracks.size();
}

json PlayQueue::enqueue(const json &args) {
    json j = {{"message-type","update"},
              {"update-type","queue"},
              {"data",{
                                      {"mode","append"},
                                      {"entries",{}}
                              }}};

    auto artist = args.find("artist");
    auto album_id = args.find("album-id");
    auto album = args.find("album");
    auto uris = args.find("uris");

    std::vector<std::string> tracks_to_add;
    if (artist != args.end()) {
        if (album != args.end()) {
            tracks_to_add = mLibrary.getTracksInAlbum(artist.value(), album.value());
        } else {
            tracks_to_add = mLibrary.getTracksByArtist(artist.value());
        }
    } else if (album_id != args.end()) {
        tracks_to_add = mLibrary.getTracksInAlbum(album_id.value());
    } else if (uris != args.end()) {
        for (auto &i : uris.value()) {
            tracks_to_add.push_back(i);
        }
    }

    int index = mTracks.size();
    int added = tracks_to_add.size();
    for (auto &i : tracks_to_add) {
        addTrack(i);
        j["data"]["entries"].push_back(mLibrary.getTrackMetadata(i).value());
    }

    mSubManager.sendUpdate("queue", j);
    return j["data"];
}

int PlayQueue::nextIndex() {
    int next_index;
    if (single_ && loop_) return mIndex;
    if (shuffle_) {
        LSFR next_rand = shuffle_rand_.getNext();
        if (shuffle_repeat_) {
            next_index = (int)(next_rand.getValue() % (uint32_t)mTracks.size());
        } else {
            if (unplayed_tracks_.empty()) {
                return -1;
            }
            auto it = unplayed_tracks_.begin();
            std::advance(it, next_rand.getValue() % unplayed_tracks_.size());
            next_index = *it;
        }
    } else {
        next_index = mIndex + 1;
    }
    if (next_index < 0 || next_index >= mTracks.size()) {
        if (loop_ && !mTracks.empty()) {
            resetUnplayedTracks();
            return 0;
        }
        return -1;
    }
    return next_index;
}

std::optional<std::string> PlayQueue::getTrackAt(int index) {
    if (index < 0 || index >= mTracks.size()) return std::nullopt;
    return mTracks[index];
}

bool PlayQueue::getShuffle() const {
    return shuffle_;
}

bool PlayQueue::setShuffle(bool shuffle) {
    shuffle_ = shuffle;
    return true;
}

std::optional<std::string> PlayQueue::nextTrack() {
    auto idx = nextIndex();
    if (idx == -1) {
        return std::nullopt;
    }
    return getTrackAt(idx);
}

void PlayQueue::resetUnplayedTracks() {
    unplayed_tracks_.clear();
    for (int i = 0; i < mTracks.size(); ++i) {
        unplayed_tracks_.insert(i);
    }
}

bool PlayQueue::setPlaybackFlags(const json &j) {
    logger_->debug("setting playback flags {}", j.dump());
    // awful logic to determine if conflicting flags are present

    bool shuffle = shuffle_;
    bool shuffle_repeat = shuffle_repeat_;
    bool consume = consume_;
    bool loop = loop_;
    bool single = single_;

    // load up the bools
    if (j.contains("shuffle"       )) shuffle = j.at("shuffle");
    if (j.contains("shuffle-repeat")) shuffle_repeat = j.at("shuffle-repeat");
    if (j.contains("consume"       )) consume = j.at("consume");
    if (j.contains("loop"          )) loop = j.at("loop");
    if (j.contains("single"          )) single = j.at("single");

    // check for conflicts
    if (single && shuffle) return false;
    if (consume && loop) return false;

    shuffle_ = shuffle;
    shuffle_repeat_ = shuffle_repeat;
    consume_ = consume;
    loop_ = loop;
    single_ = single;

    json upd = {
            {"message-type", "update"},
            {"update-type", "playback-flags"},
            {"data", getPlaybackFlags()}
    };
    mSubManager.sendUpdate("playback-flags", upd);

    return true;
}

json PlayQueue::getPlaybackFlags() {
    json j = {
            {"shuffle", shuffle_},
            {"shuffle-repeat", shuffle_repeat_},
            {"consume", consume_},
            {"loop", loop_},
            {"single", single_}
    };
    return j;
}

json PlayQueue::currentTrackInfo() {
    return trackInfo(currentIndex());
}

json PlayQueue::trackInfo(int index) {
    return getFileMetadata(mLibrary.getTrackPath(getTrackAt(index).value())).value();
}

