//
// Created by user on 6/19/19.
//

#include <iostream>
#include <spdlog/spdlog.h>
#include "subscriptionmanager.hpp"
#include "socket.hpp"


SubscriptionManager::SubscriptionManager()
{

}

void SubscriptionManager::subscribeAction(
        std::shared_ptr<ClientContext> ctx,
        uint8_t tag,
        const json &doc)
{
    auto logger = spdlog::get("cygnus");
    //std::cout << "subscribing" << std::endl;

    auto tagVal = doc.find("subscribe-to");
    if (tagVal == doc.end()) return;

    logger->debug("{} subscribing to {}", ctx->getAddress(), std::string(tagVal.value()));

    mSubscribers[tagVal.value()].push_back(ctx);
}

void SubscriptionManager::sendUpdate(const std::string &tag, json &doc) {
    auto it = mSubscribers[tag].begin();
    while (it != mSubscribers[tag].end()) {
        // check if the socket is open and send if it is
        if ((*it)->isOpen()) {
            (*it)->send(doc);
            ++it;
        } else {
            // remove ctx from subscribers
            mSubscribers[tag].erase(it);
        }
    }
}

