//
// Created by user on 12/21/19.
//

#ifndef CYGNUS_DBCURSOR_HPP
#define CYGNUS_DBCURSOR_HPP

#include <sqlite3.h>

class DBCursor {
    friend class Database;
protected:
    DBCursor(sqlite3_stmt *stmt);

public:
    ~DBCursor();
    DBCursor(DBCursor &&other);

    void step();
    bool isRow();
    bool isDone();

    int columnCount();
    const char * columnName(int index);

    int getInt(int col);
    const unsigned char * getText(int col);
    std::string getString(int col);

    DBCursor& operator=(const DBCursor &other) = delete;
private:
    sqlite3_stmt *mSTMT;
    int mRC;
    bool mFinalized;
};


#endif //CYGNUS_DBCURSOR_HPP
