//
// Created by user on 12/8/19.
//

#ifndef CYGNUS_METADATAREADER_HPP
#define CYGNUS_METADATAREADER_HPP

#include <string>
#include <map>

#include "trackmetadata.hpp"

std::optional<TrackMetadata> getFileMetadata(const std::string &path);

#endif //CYGNUS_METADATAREADER_HPP
