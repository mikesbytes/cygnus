//
// Created by user on 2/8/20.
//

#include "lsfr.hpp"
#include <sys/random.h>

LSFR::LSFR() {
    // seed lsfr with random data
    getrandom(&value_, sizeof(value_), 0);
}

LSFR::LSFR(uint32_t value) :
        value_(value)
{

}

uint32_t LSFR::getValue() const {
    return value_;
}

LSFR LSFR::getNext() {

    // 4 tap fibonacci LSFR on bits 24 23 22 17
    uint8_t bit = ((value_ >> 8) ^ (value_ >> 9) ^ (value_ >> 10) ^ (value_ >> 15)) & 1;
    LSFR res((value_ >> 1) | (bit << 31));

    return res;
}


