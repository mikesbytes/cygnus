//
// Created by user on 12/19/19.
//

#include <toml11/toml.hpp>
#include "appcontext.hpp"

AppContext::AppContext(const toml::value &conf) :
    mThreadPool(cyg::get_thread_pool("default")),
    mPlayQueue(mSubscriptionManager, mLibrary),
    mAudioProvider(*this, mPlayQueue, mSubscriptionManager),
    mOutput(mPlayQueue, mSubscriptionManager, mAudioProvider),
    mLibrary(toml::find_or<toml::value>(conf, "library", &dummy_conf_))
{

}

Library &AppContext::getLibrary() {
    return mLibrary;
}

ThreadPool &AppContext::getThreadPool() {
    return mThreadPool;
}

SubscriptionManager &AppContext::getSubscriptionManager() {
    return mSubscriptionManager;
}

PlayQueue &AppContext::getPlayQueue() {
    return mPlayQueue;
}

AudioProvider &AppContext::getAudioProvider() {
    return mAudioProvider;
}

Output &AppContext::getOutput() {
    return mOutput;
}
