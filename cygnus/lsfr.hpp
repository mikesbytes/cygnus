//
// Created by user on 2/8/20.
//

#ifndef CYGNUS_LSFR_HPP
#define CYGNUS_LSFR_HPP


#include <cstdint>

class LSFR {
public:
    LSFR();

    uint32_t getValue() const;
    void setValue(int value);

    LSFR getNext();
private:
    LSFR(uint32_t value);
    uint32_t value_;
};


#endif //CYGNUS_LSFR_HPP
