cmake_minimum_required(VERSION 3.1)
project(cygnus VERSION 0.2.0)

set(CMAKE_CXX_STANDARD 17)

find_package(Threads)

#unset(ENABLE_STATIC CACHE)
#add_subdirectory(extern/taglib)
#set(ENABLE_STATIC true CACHE BOOL "set static")

#add_subdirectory(extern/cpptui)
#add_subdirectory(extern/libcygnus)
include(conan.cmake)
conan_cmake_run(CONANFILE conanfile.txt BASIC_SETUP PROFILE default BUILD missing)

configure_file(src/cygnusconfig.h.in cygnusconfig.h)

set (common-include-dirs
        #${PROJECT_SOURCE_DIR}/extern/json/single_include
        extern/include
        "${PROJECT_BINARY_DIR}")
        #extern/libcygnus/include)

set (cygnus-sources-common
        cygnus/output.cpp
        cygnus/output.hpp
        cygnus/library.cpp
        cygnus/library.hpp
        cygnus/playqueue.cpp
        cygnus/playqueue.hpp
        cygnus/subscriptionmanager.cpp
        cygnus/subscriptionmanager.hpp
        cygnus/audioprovider.cpp
        cygnus/audioprovider.hpp
        cygnus/metadatareader.cpp
        cygnus/metadatareader.hpp
        cygnus/appcontext.cpp cygnus/appcontext.hpp cygnus/database.cpp cygnus/database.hpp cygnus/dbcursor.cpp cygnus/dbcursor.hpp cygnus/lsfr.cpp cygnus/lsfr.hpp)

set (cygnus-sources
        ${cygnus-sources-common}
        cygnus/main.cpp)

add_executable(cygnus-sv ${cygnus-sources})
target_include_directories(cygnus-sv PRIVATE "./extern/miniaudio" "./extern/rapidjson/include" "./extern/taglib/taglib" ${common-include-dirs})
target_link_libraries(cygnus-sv ${CMAKE_THREAD_LIBS_INIT} m dl ${CONAN_LIBS} tag sqlite3 z)
set_target_properties(cygnus-sv PROPERTIES OUTPUT_NAME "cygnus")
#target_compile_definitions(Cygnus PRIVATE RAPIDJSON_HAS_STDSTRING=1)

set (cygnus-tests-sources
        ${cygnus-sources-common}
        cygnus/tests/main.cpp
        cygnus/tests/database.cpp cygnus/tests/lsfr.cpp)
add_executable(cygnus-tests ${cygnus-tests-sources})
target_include_directories(cygnus-tests PRIVATE "./extern/miniaudio" "./extern/rapidjson/include" "./extern/taglib/taglib" ${common-include-dirs})
target_link_libraries(cygnus-tests ${CMAKE_THREAD_LIBS_INIT} m dl ${CONAN_LIBS} tag sqlite3 z)
#target_compile_definitions(Cygnus-tests PRIVATE RAPIDJSON_HAS_STDSTRING=1)

add_executable(cygnusc cygnusc/main.cpp)
target_include_directories(cygnusc PRIVATE "./extern/rapidjson/include" ${common-include-dirs})
target_link_libraries(cygnusc ${CMAKE_THREAD_LIBS_INIT} ${CONAN_LIBS})

install(TARGETS cygnus-sv RUNTIME DESTINATION usr/bin)
install(TARGETS cygnusc RUNTIME DESTINATION usr/bin)
install(FILES test-env/cygnus.toml DESTINATION etc/cygnus)

set(CPACK_PACKAGING_INSTALL_PREFIX "/")
set(CPACK_GENERATOR "DEB;TGZ")
set(CPACK_PACKAGE_VERSION ${PROJECT_VERSION})
set(CPACK_DEBIAN_PACKAGE_MAINTAINER "MikesBytes")
set(CPACK_DEBIAN_PACKAGE_DEPENDS "sqlite3")

include(CPack)